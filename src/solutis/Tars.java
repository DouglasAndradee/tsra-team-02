package solutis;

import robocode.ScannedRobotEvent;
import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.RobotDeathEvent;
import robocode.RoundEndedEvent;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;

import solutis.gun.LinearTargetingGun;
import solutis.movement.MinimumRisk;
import solutis.core.DataTracker;
import solutis.gun.GunStrategy;

/*
 * Processo Seletivo - Solutis
 * Autor: Jo�o Pedro Brito Silva (UFBA)
 * 
 * 15/10/2020:
 * 		Finalmente comecei a escrever meu rob� para a competi��o. Montei um projeto no eclipse e tenho algumas ideias:
 * 		
 * 		# Movimenta��o anti-gravitacional: Criar campos repulsivos ao redor de rob�s advers�rios. (Seria poss�vel extender isto para muni��es?)
 * 			Refer�ncia: https://robowiki.net/wiki/Anti-Gravity_Tutorial e DustBunny
 *
 * 		# Aprender a compensar disparos de acordo com os advers�rios. (Regress�o linear simples, apenas registrando os eventos bem / mal sucedidos)
 * 		-------------------------------------------------------------------------------------------------------------------------------------------
 * 		Fim do dia: At� posso implementar o movimento anti-gravitacional, mas acho que posso conseguir melhor com movimentos de risco m�nimo.
 * 					Classes 'DataTracker' e 'EnemyData' parcialmente implementadas, mas ainda nada de reposit�rio.
 * 
 * 16/10/2020:
 * 		Iniciei as implementa��es do MRM (Minimum Risk Movement) ontem mesmo, mas tenho apenas um algoritmo de movimenta��o aleat�ria.
 * 		Ainda preciso corrigir uma falha l�gica, mas acredito que a movimenta��o j� est� bem �gil.
 * 		
 * 17/10/2020:
 * 		Movimenta��o por risco m�nimo implementada. Ainda existem algumas falhas no planejamento, mas isto deve se resolver
 * 		com uma estrat�gia espec�fica de escaneamento. Por enquanto, o rob� apenas foge. (E consegue boas coloca��es em batalhas contra hammers).
 * 
 * 19/10/2020:
 * 		Comecei a implementa��o do algoritmo de disparo com regress�o linear e a do radar. Ambos utilizando Strategy, o que possibilita
 * 		a altera��o da estrat�gia em cen�rios com menos competidores (ou at� 1x1).
 * */
public class Tars extends AdvancedRobot{
	private MinimumRisk movement;
	private DataTracker tracker;
	private GunStrategy gun;
	
	public void onPaint(Graphics2D g) {
		movement.onPaint(g);		
	}
	
	public void run() {
		this.initComponents();
		
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);

		this.setTurnRadarLeft(Double.POSITIVE_INFINITY);
	}
	
	public void initComponents() {
		if(this.tracker == null)
			this.tracker = new DataTracker(this);

		if(this.gun == null)
			this.gun = new LinearTargetingGun(this);
		
		if(this.movement == null)
			this.movement = new MinimumRisk(this);
	}
	
	public void onScannedRobot(ScannedRobotEvent e) {
		setTurnRadarLeftRadians(getRadarTurnRemainingRadians()); //travamento do radar
		
		tracker.onScannedRobot(e);
		
		movement.updateMovement();
		gun.onScannedRobot(e);
	}
	
	public void onHitByBullet(HitByBulletEvent e) {
		movement.registerDangerousLocation(new Point2D.Double(getX(), getY()));
	}
	
	public void onRoundEnded(RoundEndedEvent e) {		
		tracker.resetEnemiesStatus();
		movement.onRoundEnded(e);
		gun.onRoundEnded(e);
	}
	
	public void onHitRobot(HitRobotEvent e) {
		movement.registerDangerousLocation(new Point2D.Double(getX(), getY()));
	}
	
	public void onRobotDeath(RobotDeathEvent e){
		tracker.onRobotDeath(e);
	}
	
	public DataTracker getTracker() {
		return this.tracker;
	}
}
