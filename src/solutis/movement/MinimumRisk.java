package solutis.movement;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.Iterator;

import robocode.util.Utils;
import solutis.Tars;
import solutis.core.DataTracker;
import solutis.core.EnemyData;

public class MinimumRisk extends MovementStrategy {
	final static int SAMPLE_COUNT = 32;
	final static double SAMPLE_OFFSET = 360.0 / (double) SAMPLE_COUNT;
	final static double SAMPLE_RADIUS = 800.0;
	final static double WALL_DISTANCE = 70;
	final static int DANGEROUS_LOCATIONS_COUNT = 10;

	private Point2D[] dangerousLocations;
	private int dangerousLocationsIndex;
	
	private Point2D[] samples;
	private double[] risks;
	
	public MinimumRisk(Tars robot) {
		super(robot);
		
		this.risks = new double[SAMPLE_COUNT];
		this.samples = new Point2D[SAMPLE_COUNT];
		
		this.dangerousLocations = new Point2D[DANGEROUS_LOCATIONS_COUNT];
		this.dangerousLocationsIndex = 0;
	}
	
	public void updateMovement() {
		Point2D destination = this.getDestination(); // Calculando a rota a cada instante.
		double x = robot.getX(), y = robot.getY();
		double heading = robot.getHeadingRadians();
		
		double angle = Utils.normalAbsoluteAngle(Math.atan2(destination.getX() - x, destination.getY() - y));
		double distance = destination.distance(x, y);
		
		if(Math.abs(angle - heading) < Math.PI / 2) {
			robot.setTurnRightRadians(Utils.normalRelativeAngle(angle - heading));
			robot.setAhead(distance);
		}
		else {
			robot.setTurnRightRadians(Utils.normalRelativeAngle(angle + Math.PI - heading));
			robot.setAhead(-distance);
		}
	}

	public Point2D getDestination() {
		double height = robot.getBattleFieldHeight();
		double width = robot.getBattleFieldWidth();
		double x = robot.getX(), y = robot.getY();
		
		double bestSampleRisk = Double.POSITIVE_INFINITY;
		Point2D bestSample = null;
		
		int i = 0;
		for(double angle = 0; angle < 360.0; angle += SAMPLE_OFFSET, i++) {
			double pX = x + Math.sin(Math.toRadians(angle)) * SAMPLE_RADIUS;
			double pY = y + Math.cos(Math.toRadians(angle)) * SAMPLE_RADIUS;
			
			pX = Math.min(width - WALL_DISTANCE, Math.max(WALL_DISTANCE, pX));
			pY = Math.min(height - WALL_DISTANCE, Math.max(WALL_DISTANCE, pY));
			
			Point2D sample = new Point2D.Double(pX, pY);
			double sampleRisk = risk(sample, angle);
			
			if(bestSample == null || (sampleRisk < bestSampleRisk)) {
				bestSample = sample;
				bestSampleRisk = sampleRisk;
			}
			risks[i] = sampleRisk;
			samples[i] = sample;
		}
//		this.registerDangerousLocation(new Point2D.Double(robot.getX(), robot.getY()));
		return bestSample;	
	}
	
	public double risk(Point2D point, double angle) {
		DataTracker tracker = robot.getTracker();
		double maxDistance = Math.sqrt(Math.pow(robot.getBattleFieldWidth(), 2)
									   + Math.pow(robot.getBattleFieldHeight(), 2));
		
		double distanceFactor = 1.0, energyFactor = 0.0, straightLineFactor = 0.0;
		double centerDistanceFactor = 1.0 - point.distance(robot.getBattleFieldWidth() / 2.0,
														   robot.getBattleFieldHeight() / 2.0) / maxDistance;
		
		Collection enemies = tracker.getEnemies();
		Iterator iterator = enemies.iterator();
		while(iterator.hasNext()) {
			EnemyData e = (EnemyData) iterator.next();
			if(e.isDead())
				continue;
			
			double distanceToEnemy = point.distance(e.getPosition());
			distanceFactor = Math.min(distanceFactor, 1.0 - distanceToEnemy / maxDistance);
			energyFactor += fitInRange(e.getEnergy() / robot.getEnergy(), 0.0, 1.0);
			
			Line2D line = new Line2D.Double(robot.getX(), robot.getY(), point.getX(), point.getY());
			if(line.intersects(e.getX(), e.getY(), robot.getWidth() * 2, robot.getHeight() * 2))
				straightLineFactor += 1.0 / tracker.getCountOfEnemiesAlive();
		}
		
		double dangerousPointsFactor = 0.0, pointsCount = 0;
		for(int i = 0; i < this.dangerousLocations.length; i++) {
			Point2D location = this.dangerousLocations[i];
			if(location == null)
				break;
			
			dangerousPointsFactor += 1.0 - location.distance(point) / maxDistance;
			pointsCount += 1.0;
		}
		pointsCount = Math.max(pointsCount, 1);
		return distanceFactor + energyFactor + (dangerousPointsFactor / pointsCount) + straightLineFactor + centerDistanceFactor;
	}
	
	// TODO: Duplicate method here. Implement some "Math lib".
	public double fitInRange(double value, double min, double max) {
		return Math.min(max, Math.max(min, value));
	}
	
	public void registerDangerousLocation(Point2D point) {
		int index = Math.max(0, 
							 (dangerousLocationsIndex - 1) % DANGEROUS_LOCATIONS_COUNT);
		
		Point2D lastLocation = dangerousLocations[index];
		if(lastLocation == null || lastLocation.distance(point) > 50.0) {
			System.out.println("REGISTERING " + point + " ON INDEX " + this.dangerousLocationsIndex);
			dangerousLocations[this.dangerousLocationsIndex++ % DANGEROUS_LOCATIONS_COUNT] = point;
		}
	}
	
	public void onPaint(Graphics2D g) {
		double minFactor = Double.POSITIVE_INFINITY, maxFactor = Double.NEGATIVE_INFINITY;
		for(int i = 0; i < SAMPLE_COUNT; i++) {
			minFactor = Math.min(minFactor, risks[i]);
			maxFactor = Math.max(maxFactor, risks[i]);
		}
		
		for(int i = 0; i < SAMPLE_COUNT; i++) {
			if(samples[i] == null)
				break;
			
			int factor = (int) (255 * (risks[i] - minFactor) / (maxFactor - minFactor));
			
			g.setColor(new java.awt.Color(factor, 0, 255 - factor));
			this.drawPoint(samples[i], g);
		}
		
		for(int i = 0; i < DANGEROUS_LOCATIONS_COUNT; i++) {
			if(this.dangerousLocations[i] == null)
				break;
			
			g.setColor(new java.awt.Color(0, 128, 0));
			this.drawPoint(dangerousLocations[i], g);
		}
	}
}
