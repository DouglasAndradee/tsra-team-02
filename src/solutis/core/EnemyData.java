package solutis.core;

import java.awt.geom.Point2D;

import robocode.ScannedRobotEvent;

public class EnemyData{		
	private boolean isDead = false;

	private double absoluteBearingRadians;
	private double headingRadians;
	private double bearingRadians;
	private double velocity;
	private double distance;
	private double heading;
	private double energy;

	private DataTracker tracker;
	private Point2D position;
	private String name;
	
	public EnemyData(DataTracker tracker, ScannedRobotEvent e) {
		this.tracker = tracker;
		this.updateDataFromScanEvent(e);
	}
	
	public void updateDataFromScanEvent(ScannedRobotEvent e) {		
		this.name = e.getName();
		
		this.absoluteBearingRadians = tracker.getHeadingRadians() + e.getBearingRadians();
		this.headingRadians = e.getHeadingRadians();
		this.bearingRadians = e.getBearingRadians();
		
		this.distance = e.getDistance();
		this.velocity = e.getVelocity();
		this.heading = e.getHeading();
		this.energy = e.getEnergy();
		
		this.position = new Point2D.Double(tracker.getX() + Math.sin(this.absoluteBearingRadians) * this.distance,
										   tracker.getY() + Math.cos(this.absoluteBearingRadians) * this.distance);
	}
	
	public double getHeadingRadians() {
		return this.headingRadians;
	}
	
	public double getHeading() {
		return this.heading;
	}
	
	public double getBearing() {
		return this.bearingRadians;
	}
	
	public double getAbsoluteBearingRadians() {
		return this.absoluteBearingRadians;
	}
	
	public double getDistance() {
		return this.distance;
	}
	
	public double getEnergy() {
		return this.energy;
	}
	
	public String getName() {
		return this.name;
	}
	
	public double getVelocity() {
		return this.velocity;
	}
	
	public Point2D getPosition() {
		return this.position;
	}
	
	public double getX() {
		return this.position.getX();
	}
	
	public double getY() {
		return this.position.getY();
	}
	
	public boolean isDead() {
		return this.isDead;
	}
	
	public void setDeathStatus(boolean isDead) {
		this.isDead = isDead;
	}
}