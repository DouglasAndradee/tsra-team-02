package solutis.core;

import robocode.RobotDeathEvent;
import robocode.RoundEndedEvent;
import robocode.ScannedRobotEvent;
import solutis.Tars;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

// Implementing data tracker with Singleton project pattern.
public class DataTracker {	
	private HashMap enemies = null;
	private Tars robot;
	
	private int enemiesAlive;
	
	public DataTracker(Tars robot) {
		this.enemies = new HashMap();
		this.robot = robot;
		
		this.enemiesAlive = 0;
	}
	
	public void onScannedRobot(ScannedRobotEvent e) {
		EnemyData enemy = (EnemyData) this.enemies.get(e.getName());
		if(enemy == null) {
			this.enemies.put(e.getName(), new EnemyData(this, e));
			this.enemiesAlive += 1;
		}
		else
			enemy.updateDataFromScanEvent(e);
	}
	
	public void onRobotDeath(RobotDeathEvent e) {
		EnemyData enemy = (EnemyData) this.enemies.get(e.getName());
		if(enemy != null) {
			enemy.setDeathStatus(true);
			this.enemiesAlive -= 1;
		}
	}
	
	public void resetEnemiesStatus() {
		
		Collection enemies = this.enemies.values();
		Iterator iterator = enemies.iterator();
		while(iterator.hasNext()) {
			EnemyData enemy = (EnemyData) iterator.next();
			enemy.setDeathStatus(false);
		}
		
		this.enemiesAlive = this.enemies.size();
	}
	
	public void onRoundEnded(RoundEndedEvent e) {
		this.resetEnemiesStatus();
	}
	
	public void onRoundStart() {
		this.resetEnemiesStatus();
	}
	
	public Point2D getPosition() {
		return new Point2D.Double(robot.getX(), robot.getY());
	}
	
	public double getX(){
		return robot.getX();
	}
	
	public double getY(){
		return robot.getY();
	}
	
	public double getHeadingRadians() {
		return robot.getHeadingRadians();
	}
	
	public Collection getEnemies(){
		return this.enemies.values();
	}
	
	public int getCountOfEnemies() {
		return this.enemies.size();
	}
	
	public int getCountOfEnemiesAlive() {		
		return this.enemiesAlive;
	}
	
	public double getBattleFieldWidth() {
		return robot.getBattleFieldWidth();
	}
	
	public double getBattleFieldHeight() {
		return robot.getBattleFieldHeight();
	}
	
	public double getEnergy() {
		return robot.getEnergy();
	}
	
	public double getGunHeadingRadians() {
		return robot.getGunHeadingRadians();
	}
	
	public EnemyData getNearestEnemy() {
		EnemyData nearestEnemy = null;
		
		Collection enemies = this.enemies.values();
		Iterator iterator = enemies.iterator();
		while(iterator.hasNext()) {
			EnemyData enemy = (EnemyData) iterator.next();
			if(enemy.isDead())
				continue;
			
			if(nearestEnemy == null || nearestEnemy.getDistance() > enemy.getDistance())
				nearestEnemy = enemy;
		}
		return nearestEnemy;
	}
	
	public double getNearestEnemyDistance() {
		EnemyData enemy = this.getNearestEnemy();
		if(enemy != null)
			return enemy.getDistance();
		else
			return 200.0;
	}
	
	public String getNearestEnemyName() {
		EnemyData enemy = getNearestEnemy();
		if(enemy == null)
			return null;
		
		return enemy.getName();
	}

	public boolean isNearestEnemy(String name) {
		return name.equals(getNearestEnemyName());
	}
}
